Source: qtcurve
Section: kde
Priority: optional
Maintainer: Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
Uploaders: Boris Pek <tehnick@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 12),
               extra-cmake-modules,
               gettext,
               libcairo2-dev,
               libgtk2.0-dev,
               libkf5archive-dev,
               libkf5config-dev,
               libkf5configwidgets-dev,
               libkf5guiaddons-dev,
               libkf5i18n-dev,
               libkf5iconthemes-dev,
               libkf5kdelibs4support-dev,
               libkf5kio-dev,
               libkf5style-dev,
               libkf5widgetsaddons-dev,
               libkf5windowsystem-dev,
               libkf5xmlgui-dev,
               libqt5svg5-dev,
               libqt5x11extras5-dev,
               libx11-dev,
               libx11-xcb-dev,
               libxcb1-dev,
               pkg-config,
               pkg-kde-tools,
               qtbase5-dev,
               qtbase5-private-dev
Homepage: https://invent.kde.org/system/qtcurve
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/qtcurve.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/qtcurve
Standards-Version: 4.5.0
Rules-Requires-Root: no

Package: gtk2-engines-qtcurve
Section: gnome
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends}
Recommends: kde-style-qtcurve-qt5
Multi-Arch: same
Description: QtCurve widget style for applications based on GTK+ 2.x
 This package contains the QtCurve theme engine for GTK+ 2.x.
 .
 This package is most useful when installed together with kde-style-qtcurve.
 .
 QtCurve is a set of widget styles for Qt and GTK+ libraries. It provides a
 consistent look between KDE, GNOME and other applications based on these
 libraries, which is easy on the eyes and visually pleasing.

Package: kde-style-qtcurve-qt5
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: gtk2-engines-qtcurve
Multi-Arch: same
Description: QtCurve widget style for applications based on Qt 5.x
 This package contains the QtCurve widget style for Qt 5.x.
 .
 The corresponding GTK+ theme engine can be found in gtk2-engines-qtcurve
 package.
 .
 QtCurve is a set of widget styles for Qt and GTK+ libraries. It provides a
 consistent look between KDE, GNOME and other applications based on these
 libraries, which is easy on the eyes and visually pleasing.

Package: qtcurve-l10n
Section: localization
Architecture: all
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: translation files for QtCurve
 This package contains translation files for QtCurve.
 .
 QtCurve is a set of widget styles for Qt and GTK+ libraries. It provides a
 consistent look between KDE, GNOME and other applications based on these
 libraries, which is easy on the eyes and visually pleasing.

Package: qtcurve
Section: metapackages
Architecture: any
Depends: gtk2-engines-qtcurve,
         kde-style-qtcurve-qt5,
         qtcurve-l10n,
         ${misc:Depends}
Suggests: kwin-decoration-oxygen, oxygen-icon-theme
Multi-Arch: same
Description: unified widget styles for Qt and GTK+ applications (metapackage)
 This package installs all packages related with QtCurve.
 .
 QtCurve is a set of widget styles for Qt and GTK+ libraries. It provides a
 consistent look between KDE, GNOME and other applications based on these
 libraries, which is easy on the eyes and visually pleasing.

Package: libqtcurve-utils2
Section: libs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Replaces: libqtcurve-utils1 (>= 1.8.18+git20150711-a3fff13-1)
Breaks: libqtcurve-utils1 (>= 1.8.18+git20150711-a3fff13-1)
Multi-Arch: same
Description: common library for QtCurve
 This package contains common library for QtCurve.
 .
 QtCurve is a set of widget styles for Qt and GTK+ libraries. It provides a
 consistent look between KDE, GNOME and other applications based on these
 libraries, which is easy on the eyes and visually pleasing.
